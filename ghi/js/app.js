function createCard(title, description, pictureUrl, start, end, location) {
  return `
  <div class="col shadow p-3 mb-5 bg-white rounded" style="margin-bottom: 20px;">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top" alt="...">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
      </div>
      <div class="card-footer bg-transparent border-success">${start} - ${end}</div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok');

      } else {
        const data = await response.json();
        const column = document.querySelectorAll('.col');
        let index = 0;

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            if (detailResponse.ok) {
              const details = await detailResponse.json();
              // console.log(details)

              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const start = new Date(details.conference.starts).toLocaleDateString();
              const end = new Date(details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;

              const html = createCard(title, description, pictureUrl, start, end, location);
              column[index].innerHTML += html;
              if (index < 2) {
                index += 1;
              } else {
                index = 0;
              }
              }
          }
        }
    } catch (e) {
        console.error('error:', e)
    }

  });
