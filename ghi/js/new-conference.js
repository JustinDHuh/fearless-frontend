window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        const option = document.createElement('option');

        option.value = location.abbreviation;
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    }

    // Form submission code
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();

      // Get form data and convert it to JSON
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      // Submit the form data to the API
      const locationUrl = 'http://localhost:8000/api/locations/';
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
          formTag.reset();
          const newLocation = await response.json();
        } else {
          console.error('Error submitting the form:', response.status, response.statusText);
          // Handle the error appropriately
        }
      } catch (error) {
        console.error('An  error occurred:', error);
        // Handle unexpected errors
      }
    });
  });
